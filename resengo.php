<?php

/**
 * Plugin Name:       Resengo
 * Plugin URI:        https://www.spotdesign.be/wordpress/resengo/
 * Description:       Embed Resengo widgets using shortcode's and / or anchor tags.
 * Version:           1.0.2
 * Author:            Mathias De Beyser
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       resengo
 */

// Check that the file is not accessed directly.
if( !defined( 'ABSPATH' ) ) {
    die( 'We\'re sorry, but you can not directly access this file.' );
}

if( !class_exists( 'Resengo' ) ) {
    class Resengo {

        /**
         * Vars
         */
        private $_domain;
        private $_options;


        /**
         * Construct
         */
        public function __construct() {

            //= setup vars
            $this->_domain  = 'resengo';
            $this->_options = array(
                (object) array(
                    'id'    => 'company_id',
                    'label' => _x( 'Company ID', 'option label', $this->_domain ),
                    'desc'  => 'Jouw Resengo Company ID',

                    'args'  => array(
                        'type'    => 'number',
                        'default' => 0
                    )
                ),

                //= TODO
                (object) array(
                    'id'    => 'script',
                    'label' => _x( 'Script', 'option label', $this->_domain ),
                    'desc'  => 'Prefereer script integratie boven iframe integratie',

                    'args'  => array(
                        'type'    => 'boolean',
                        'default' => 0
                    )
                ),

                (object) array(
                    'id'    => 'debug',
                    'label' => _x( 'Debugging', 'option label', $this->_domain ),
                    'desc'  => 'Schakel debuggin in',

                    'args'  => array(
                        'type'    => 'boolean',
                        'default' => 0
                    )
                )
            );

            //= attach hooks
            add_action( 'init', array( $this, 'init' ) );
            add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

            add_action( 'admin_init', array( $this, 'admin_init' ) );
            add_action( 'admin_menu', array( $this, 'admin_menu' ) );

            add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
            add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_links' ), 10, 2 );

        }


        /**
         * Hook: plugins_loaded()
         */
        public function plugins_loaded() {

            load_plugin_textdomain( 'resengo', false, basename( dirname( __FILE__ ) ) . '/languages' );

        }


        /**
         * Hook: init()
         */
        public function init() {

            //= create shortcodes
            add_shortcode( 'resengo', array( $this, 'shortcode_callback' ) );

            //= allow 3rd party to hook in
            do_action( 'resengo/init' );

        }


        /**
         * Hook: wp_enqueue_scripts()
         */
        public function enqueue_scripts() {

            //= resengo libs
            wp_register_script( 'lib-resengo' , "https://www.resengo.com/WID/Widget/Cors", array(), false, true );

            //= fancybox libs
            // if( $this->get_setting( 'include_fancybox' ) ) :
            //     wp_register_style( 'lib-fancybox', "https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css", array(), '3.5.7' );
            //     wp_register_script( 'lib-fancybox', "https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js", array( 'jquery' ), '3.5.7', true );
            // endif;

            //= resengo plugin
            wp_register_style( 'wp-resengo', plugins_url( 'assets/css/resengo.css', __FILE__ ), array( 'lib-fancybox' ) );
            wp_enqueue_style( 'wp-resengo' );

            wp_register_script( 'wp-resengo' , plugins_url( 'assets/js/resengo.js', __FILE__ ), array( 'lib-resengo', 'lib-fancybox' ), false, true );
            wp_enqueue_script ( 'wp-resengo' );

            wp_localize_script( 'wp-resengo', 'Resengo', array(
                'debug'     => $this->get_setting( 'debug' ),
                'script'    => $this->get_setting( 'script' ),
                'lang'      => $this->get_curr_lang(),
                'companyID' => $this->get_setting( 'company_id' ),

                'itafelUrl' => $this->get_itafel_url(),
                'ishopUrl'  => $this->get_ishop_url()
            ) );

        }


        /**
         * Hook: admin_init()
         */
        public function admin_init() {

            foreach( $this->_options as $option ) {
                register_setting( $this->_domain, "{$this->_domain}/{$option->id}" );
            }

        }


        /**
         * Hook: admin_menu()
         */
        public function admin_menu() {

            $page_title = __( 'Resengo', 'resengo' );
            $menu_title = __( 'Resengo', 'resengo' );
            $capability = apply_filters( 'resengo/manage-capability', 'edit_posts' );
            $menu_slug  = 'resengo-settings';

            add_options_page( $page_title, $menu_title, $capability, $menu_slug, array( $this, 'render_page_settings' ) );

        }


        /**
         * Hook: plugin_action_links()
         */
        public function plugin_links( $links, $file ) {

            $new_links = array(
                '<a href="' . admin_url( 'options-general.php?page=resengo-settings' ) . '">' . __( 'Settings', 'resengo' ) . '</a>'
            );

            return array_merge( $links, $new_links );

        }


        /**
         * Callback: add_options_page() renderer
         */
        public function render_page_settings() { ?>

            <style>
                .wrap {
                    max-width: 640px;
                }

                .wrap h4 {
                    margin-top: 0;
                    margin-bottom: 0;
                }

                #resengo-logo {
                    max-width: 120px;
                }

                #resengo-settings form small {
                    color: gray;
                    font-weight: normal;
                    display: block;
                }

                .section {
                    margin-top: 32px;
                    background-color: #fff;
                }

                .section .section-header {
                    color: #fff;
                    padding: 10px 20px;
                    background-color: #2684e0;
                }

                .section .section-content {
                    padding: 20px;
                }

                .section .section-content .form-table {
                    margin-top: 0;
                }

                .section .section-content article + article {
                    margin-top: 32px;
                }
            </style>

            <div class="wrap">
                <h1><img id="resengo-logo" src="<?= plugins_url( "/assets/images/resengo-logo.svg", __FILE__ ); ?>" alt="<?php _e( 'Resengo', 'resengo' ); ?>" /></h1>

                <section id="resengo-settings" class="section">
                    <div class="section-header">
                        <h4 class="section-title"><?php _e( 'Settings', 'resengo' ); ?></h4>
                    </div>

                    <div class="section-content">
                        <form method="post" action="options.php">
                            <?php settings_fields( $this->_domain ); ?>
                            <?php do_settings_sections( $this->_domain ); ?>

                            <table class="form-table">

                                <?php foreach( $this->_options as $option ) : ?>
                                    <tr valign="top">
                                        <th scope="row">
                                            <label><?= $option->label; ?></label>
                                            <?php if( isset( $option->desc ) && !empty( $option->desc ) ) : ?><small><?= $option->desc; ?></small><?php endif; ?>
                                        </th>
                                        <td>

                                            <?php if( $option->args[ 'type' ] == 'boolean' ) : ?>
                                                <?php $value = $this->get_setting( $option->id ) ? 1 : 0; ?>
                                                <input type="checkbox" name="<?= esc_attr( "{$this->_domain}/{$option->id}" ); ?>" <?= $value ? "checked" : ""; ?> />

                                            <?php else : ?>
                                                <input type="text" name="<?= esc_attr( "{$this->_domain}/{$option->id}" ); ?>" value="<?= esc_attr( $this->get_setting( $option->id ) ); ?>" />

                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </table>

                            <?php submit_button(); ?>
                        </form>
                    </div>
                </section>


                <section id="resengo-examples" class="section">
                    <div class="section-header">
                        <h4 class="section-title"><?php _e( 'Widgets', 'resengo' ); ?></h4>
                    </div>

                    <div class="section-content">

                        <?php if( $company_id = $this->get_setting( 'company_id' ) ) : ?>
                            <article class="example">
                                <h4 style="margin-bottom:0;">Reserveringswidget weergeven</h4>
                                <p style="margin-top:0;">Integreer de reserveringsflow a.d.h.v. onderstaande shortcode</p>

                                <p>Standaard integratie<br><code>[resengo widget="itafel"]</code></p>
                                <p>All options integratie<br><code>[resengo widget="itafel" company_id="<?= $company_id; ?>" lang="<?= $this->get_curr_lang(); ?>"]</code></p>
                            </article>

                            <article class="example">
                                <h4 style="margin-bottom:0;">Reserveringswidget call-to-action</h4>
                                <p style="margin-top:0;">Stel volgende waarde in als url van je link of button.</p>

                                <code>#resengo-booknow</code>
                            </article>

                            <article class="example">
                                <h4 style="margin-bottom:0;">Webshop weergeven</h4>
                                <p style="margin-top:0;">Integreer de webshop a.d.h.v. onderstaande shortcode</p>

                                <p>Standaard integratie<br><code>[resengo widget="ishop"]</code></p>
                                <p>All options integratie<br><code>[resengo widget="ishop" company_id="<?= $company_id; ?>" lang="<?= $this->get_curr_lang(); ?>"]</code></p>
                            </article>

                            <article class="example">
                                <h4 style="margin-bottom:0;">Webshop call-to-action</h4>
                                <p style="margin-top:0;">Stel volgende waarde in als url van je link of button.</p>

                                <code>#resengo-shopnow</code>
                            </article>

                        <?php else : ?>
                            <p>Om de widgets te kunnen gebruiken moet je het Company ID invullen.</p>

                        <?php endif; ?>

                    </div><!-- /.section-content -->
                </section><!-- /.section -->

                <section id="docs" class="section">
                    <div class="section-header">
                        <h4 class="section-title"><?php _e( 'Widgets', 'resengo' ); ?></h4>
                    </div>

                    <div class="section-content">
                        <p>Meer documentatie vind je <a href="https://www.resengo.com/NL/WID/Widget/Index?nw=1&CompanyID=<?= $this->get_setting( 'company_id' ); ?>" target="_blank">hier</a> op de offici&euml;le website van Resengo.</p>
                    </div>
                </section>


            </div><!-- /.wrap -->

        <?php }


        /**
         * Callback: Shortcode
         */
        public static function shortcode_callback( $atts, $content = "" ) {

            //= parse attributes
            $a = shortcode_atts( array(
                "widget"     => "itafel",
                "company_id" => $this->get_setting( 'company_id' ),
                "lang"       => $this->get_curr_lang()
            ), $atts );

            //= parse widget type
            switch( $a[ 'widget' ] ) {
                case 'ishop' :
                    return $this->get_iframe( array(
                        'src'    => $this->get_ishop_url( $a ),
                        'height' => 500
                    ) );

                case 'itafel' :
                default :

                    if( $this->get_setting( 'script' ) ) :
                        return '<div id="resengo-booknow--' . uniqid() . '"></div>';

                    else :
                        return $this->get_iframe( array(
                            'src'    => $this->get_itafel_url( $a ),
                            'height' => 1000
                        ) );

                    endif;

            }

        }


        /**
         * Method: get_option from wp_options table
         */
        public function get_setting( $key, $default = false ) {
            return get_option( "{$this->_domain}/{$key}", $default );
        }

        /**
         * Method: get current language based on WPML language or WP locale
         */
        public function get_curr_lang() {

            //= get language code
            $language_code = defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE : get_locale();
            $parts = explode( '_', $language_code );

            //= convert to ISO 639-1 language code
            $iso639_language_code = strtoupper( $parts[ 0 ] );

            //= return
            return $iso639_language_code;

        }

        public function get_itafel_url( $args = array() ) {

            //= parse args
            $defaults = array(
                "company_id" => $this->get_setting( 'company_id' ),
                "lang"       => $this->get_curr_lang()
            );

            $args = wp_parse_args( $args, $defaults );

            //= compose link
            $url = "https://www.resengo.com/Integration/Book";
            $query = http_build_query( array(
                "CompanyId" => $args[ 'company_id' ],
                "ForceLC"   => $args[ 'lang' ]
            ) );

            return $url . "?" . $query;

        }

        public function get_ishop_url( $args = array() ) {

            //= parse args
            $defaults = array(
                "company_id" => $this->get_setting( 'company_id' ),
                "lang"       => $this->get_curr_lang()
            );

            $args = wp_parse_args( $args, $defaults );

            //= compose link
            $url = "https://www.resengo.com/Code/Webshop/WS_Shop.asp?AID=1";
            $query = http_build_query( array(
                "AID"       => "1",
                "CompanyID" => $args[ 'company_id' ],
                "LC"        => $args[ 'lang' ]
            ) );

            return $url . "?" . $query;

        }

        public function get_iframe( $args = array() ) {

            //= handle args
            $defaults = array(
                "name"        => "Resengo",
                "width"       => "100%",
                "src"         => $this->get_itafel_url(),

                "class"       => "",
                "style"       => "max-width: 550px; background: transparent;",

                "frameborder" => 0,
                "scrolling"   => "yes",
                "allowtransparency" => "true"
            );

            $args = wp_parse_args( $args, $defaults );

            //= handle html attributes
            $attrs = "";
            foreach( $args as $key => $value ) {
                $attrs .= "$key=\"" . esc_attr( $value ) . "\"";
            }

            //= return iframe
            return "<iframe {$attrs}></iframe>";

        }

    }

    if( !function_exists( 'resengo' ) ) {
        function resengo() {
            global $resengo;

            if( !isset( $resengo ) ) {
                $resengo = new Resengo();
            }

            return $resengo;
        }
    }

    resengo();

}
