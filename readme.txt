=== Resengo ===
Contributors: mathiasdb
Requires at least: 4.8
Tested up to: 5.2
Requires PHP: 7.1
Stable tag: trunk


== Description ==
Embed Resengo widgets using shortcode's or anchor tags.


== Changelog ==

= 1.0.1 =
* New: plugin settings link in plugins table
* New: uninstall file, deletes options after uninstalling plugin
* New: readme.txt

= 1.0.0 =
* Initial commit
