<?php

//@see https://developer.wordpress.org/plugins/plugin-basics/uninstall-methods/#method-2-uninstall-php

//= if uninstall.php is not called by WordPress, die
if( !defined( 'WP_UNINSTALL_PLUGIN' ) ) die;

//= options to delete
$domain  = "resengo";
$options = array( "company_id", "script", "debug" );

foreach( $options as $option ) {
    delete_option( "{$domain}/{$option}" );
}
