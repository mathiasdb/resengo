( function( $, window ) {


    /**
     * Methods
     */
    function uniqid() {
        return Math.random().toString( 36 ).substr( 2, 9 );
    }

    function toggle_booknow_modal( e ) {

        //= prevent default anchor event
        e.preventDefault();

        //= debugging
        if( Resengo.debug ) console.log( 'resengo.js - toggle_booknow_modal()' );

        //= widget type
        if( Resengo.script && $( '#modal--resengo-booknow' )[0] ) {

            //= open modal (inline)
            $.fancybox.open({
                src  : '#modal--resengo-booknow',
                type : 'inline',

                slideClass : 'resengo-modal'
            });

        } else {

            //= open modal (iframe)
            $.fancybox.open({
                src  : Resengo.itafelUrl,
                type : 'iframe',
                slideClass : 'resengo-modal',

                iframe : {
                    css : {
                        width: '550px'
                    }
                }
            });

        }

        //= stop event
        return;

    }

    function toggle_shopnow_modal( e ) {

        //= prevent default anchor event
        e.preventDefault();

        //= debugging
        if( Resengo.debug ) console.log( 'resengo.js - toggle_shopnow_modal()' );

        //= open modal
        $.fancybox.open({
            src  : Resengo.ishopUrl,
            type : 'iframe',
            slideClass : 'resengo-modal',

            iframe : {
                css : {
                    width: '550px'
                }
            }
        });

        //= stop event
        return;

    }

    /**
     * Document Ready
     */
    $( function() {

        if( Resengo.debug ) {
            console.log( 'resengo.js - document.ready()' );
            console.log( Resengo.script );
        }

        //= check if fancybox lib is available
        if( typeof $.fn.fancybox != 'function' ) return;

        //= prefer script integration above iframe integration
        if( Resengo.script ) {

            //= add booknow container
            if( $( '[href$="#resengo-booknow"]' )[0] ) {
                var modal_id = "resengo-booknow--" + uniqid();

                $( 'body' ).append( '' +
                    '<div style="display: none;" id="modal--resengo-booknow">' +
                        '<div id="' + modal_id + '"></div>' +
                    '</div>'
                );
            }

            //= render each resengo booknow flow
            if( typeof bookNow == 'function' ) {

                $( 'div[id^="resengo-booknow"]' ).each( function( i, el ) {

                    var target = "#" + $( el ).attr( 'id' );

                    bookNow({
                        companyID : Resengo.companyID,
                        target    : target,
                        language  : Resengo.lang
                    });

                } );

            }

        }

        //= attach event listeners
        $( '[href$="#resengo-booknow"]' ).on( 'click', toggle_booknow_modal );
        $( '[href$="#resengo-shopnow"]' ).on( 'click', toggle_shopnow_modal );

    } );

} )( jQuery, window );
